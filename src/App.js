import React, { Component } from 'react';
import img from './images/profile-photo.png';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="test-im">
        <div className="col-6 artists-content p-4">
          <img src={img} className="profile-cover d-flex m-auto" alt="profile cover" />
          <h3 className="text-center mt-3">Lista de artista</h3>

          <ul>
            <li>Gustavo lima</li>
          </ul>
        </div>
      </div>
    );
  }
}

export default App;
